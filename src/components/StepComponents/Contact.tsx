import { Box, Typography } from '@mui/material'
import { FormInput } from '../FormInput'

export const Contact = () => {
  return (
    <Box marginY={10}>
      <Typography variant="h6">Contato</Typography>

      <Box display="flex" flexDirection="column" gap={2} marginTop={3}>
        <FormInput name="mobileNumber" label="Celular" />
        <FormInput name="telNumber" label="Telefone" />
      </Box>
    </Box>
  )
}
