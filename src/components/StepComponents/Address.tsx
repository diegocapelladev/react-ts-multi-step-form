import { Box, Typography } from '@mui/material'
import { FormInput } from '../FormInput'

export const Address = () => {
  return (
    <Box marginY={10}>
      <Typography variant="h6">Endereço</Typography>

      <Box display="flex" flexDirection="column" gap={2} marginTop={3}>
        <FormInput name="street" label="Rua" />
        <FormInput name="numberHouse" label="Número" />
        <FormInput name="neighborhood" label="Bairro" />
        <FormInput name="city" label="Cidade" />
      </Box>
    </Box>
  )
}
