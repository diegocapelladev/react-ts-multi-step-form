import { FormProvider, useForm } from 'react-hook-form'
import { Box, Button, Typography } from '@mui/material'
import { zodResolver } from '@hookform/resolvers/zod'
import * as z from 'zod'

import { Steps } from './Stepper'
import { PersonalInfo } from './StepComponents/PersonalInfo'
import { Address } from './StepComponents/Address'
import { Contact } from './StepComponents/Contact'

const schema = z.object({
  name: z.string().min(1).max(50, 'O máximo de caracteres permitido é 50'),
  age: z.string().min(1),

  street: z.string().min(1),
  numberHouse: z.string().min(1),
  neighborhood: z.string().min(1),
  city: z.string().min(1),

  mobileNumber: z.string().min(1),
  telNumber: z.string().min(1)
})

type FormValues = z.infer<typeof schema>

const sourceSteps = [
  {
    label: 'Dados Pessoais',
    Component: <PersonalInfo />,
    fields: ['name', 'age'],
    hasError: false
  },
  {
    label: 'Dados de Cobrança',
    Component: <Address />,
    fields: ['street', 'numberHouse', 'neighborhood', 'city'],
    hasError: false
  },
  {
    label: 'Dados de Contato',
    Component: <Contact />,
    fields: ['mobileNumber', 'telNumber'],
    hasError: false
  }
]

const getSteps = (errors: string[]) => {
  return sourceSteps.map((step) => {
    return {
      ...step,
      hasError: errors.some((error) => step.fields.includes(error))
    }
  })
}

export function Form() {
  const methods = useForm<FormValues>({
    resolver: zodResolver(schema),
    criteriaMode: 'all',
    defaultValues: {
      name: '',
      age: '',
      street: '',
      numberHouse: '',
      neighborhood: '',
      city: '',
      mobileNumber: '',
      telNumber: ''
    }
  })

  if (methods.formState.isSubmitSuccessful) {
    return (
      <Box>
        <Typography variant="h2">Formulário enviado com sucesso!</Typography>
        <Button onClick={() => methods.reset()}>
          Clique aqui para fazer um novo cadastro
        </Button>
      </Box>
    )
  }

  const steps = getSteps(Object.keys(methods.formState.errors))

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit((data) => console.log(data))}>
        <Steps items={steps} />
      </form>
    </FormProvider>
  )
}
