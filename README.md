# Multi Step Form

## Getting Started

```bash
npm run start
# or
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

<p align="center">
  <img alt="projeto Multi Step Form" src=".github/multi-step-form.png" width="100%">
</p>
